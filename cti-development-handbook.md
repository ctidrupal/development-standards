# CTI Drupal Development Handbook
## Contents

1. Introduction
2. Base Build
3. Directory Structure
4. Coding Standards
5. Version Control
6. Development & Stage Environments
7. Drush
8. Features & Custom Modules
9. Patching
10. Sites as Installation Profiles
11. Automated Testing
12. Deployment


## Introduction
When building complex Drupal websites, in large teams it is important to ensure that a strict set of standards are followed. This ensures that our deadlines are met, while maximising quality and reducing technical debt. This handbook also allows new developers and contractors to quickly get up to speed on the methods that CTI Digital uses.

## Base Build
The first step in all CTI projects is committing the base build to the git repository. Base builds initially stem from a Drush make file, which can be found [where ... ?]. This make file contains the most commonly used contributed modules, custom CTI modules, the Omega base theme and the CTI Base theme. 
All CTI projects have a 6 character short name, made up of three letters and two numbers. This is used to identify the project in Redmine, Git and as a namespace for custom modules and features.

## Directory Structure
Project repositories follow a defined structure to allow ease of automated deployment and local development. In the root of the repository the following folders may be created:

* *site/* - The Drupal site files.
* *env/* - Holds environment specific configuration files, such as _enviroment_.settings.php.
* *config/* - Build configuration files.
* *docs/* - Contains technical documents, such as the technical specification.
* *assets/* - Raw assets such as .psd files and content to be imported.

All projects are built through an installation profile, so in theory, any project can be installed without any reliance on a database. This means that all contributed and custom modules, features and themes must be placed in the profiles directory. The machine name of the installation profile should be the shortname of the project. Within the profile directory, modules must be placed into the modules/ directory, which should then be split into contrib/, custom/ and features/. Themes are to be placed into the themes/ directory, libraries in the libraries/ directory and patches into the patches/ directory. Below is an example directory structure:

		env/
		docs/
		site/
			misc/
			modules/
			profiles/
				bri002/
					modules/
						contrib/
						features/
						custom/
					libraries/
					themes/
					patches/
			sites/
				default/
				all/
					drush/
			themes/

### Environment Settings
Drupal settings, such as database credentials are stored in the `settings.php` file. As this file contains sensitive information and environment specific settings it shouldn't be version controlled. Do cater for this requirement we use the following system:

In the `env/` directory we store a file for each environment:

* `local.settings.php` - Boilerplate for local development.
* `stage.settings.php` - The settings for the stage environment. **This shouldn't contain the username and password of the database.**
* `live.settings.php` - The settings for the live environment. **This shouldn't contain the username and password of the database.**

These files are then included into the main `settings.php` file with the following code at the end of the file:

	if (file_exists('./'. conf_path() .'/	local.settings.php')) {
  		include_once './'. conf_path() .'/local.settings.php';
	}

When developing locally, you should use the `local.settings.php` boilerplate to create your own `local.settings.php` in the `sites/*/` directory. **This newly created file should not be in version control.**

### Naming Conventions
All custom modules (including features) should be prefixed with the project shortname, then an underscore (e.g. `bri002_`). Following this strict naming convention reduces the possibility of namespace clashes with contributed modules. Any modules that have been created by CTI for use on multiple projects (most should be contributed back to the community) should be prefixed by `cti_`.

## Coding Standards
All code produced should conform to the [coding standards](http://drupal.org/coding-standards) set out by the Drupal community. Additionally all CSS, no matter what version of Drupal being used conforms to the CSS formatting guidelines and additional Drupal 8 standards.
Tools such as the Drupal Coder module and PHP CodeSniffer can automate this process, for example PHP CodeSniffer could be run locally before any code is committed to the repository.

### Notices & Warnings
On local machines and the dev build the 'Error messages to diplay' setting on `admin/config/development/logging` should be set to 'All messages'. All warnings or errors should be fixed when they appear so that when we need to see the error messages we can find the relevant ones easily (as they will be the only ones).

Leaving warnings to show on every page because "it's not really a problem" or "it's only an undefined index" leads to sloppy coding as other developers will see all the current warnings and feel that one more isn't a problem. Soon there are 15 warnings on every page and we're missing the important messages or missing UI problems because everything is halfway down a page.

Fixing all warnings will also improve performance as each warning gets written to the watchdog log. Simply hiding the messages from the UI doesn't stop this and the time can become non-negligible. Again, having an uncluttered watchdog can be hugely beneficial when it comes to debugging problems.

## Version Control
We use the Git version control system to manage source code revisions. Commits must be made as often as possible, after every isolated change or addition and at least once per hour. Changes should also be regularly pushed to the development branch, particularly when modifying features.

### Cloning a Project
All Redmine projects have an associated Git repository. In order to clone the project you should use the following command:
	git clone --branch=[branch] git@git.ctidigital.com:[project-machine-name]

### Branching
We use a simple branching system for managing development, staging and live versions of a project. The master branch should always be clean, containing only a readme.txt warning that code shouldn't be committed to the master branch. This allows us to explicitly name the branches that we use, which should limit any confusion. Most repositories will have the following branches:

* **development** - The latest, possibly unstable state of current development.
* **stage** - The latest stable version of the project, ready for QA.
* **production** - This branch should always contain the release ready, stable codebase.

Code must always work it's way from the development branch, through stage and then production. Code should never be committed directly to the stage or production branches. This simple workflow leaves the local git workflow up to the developer.

### What not to Version Control
There are many files that shouldn't be included in the Git repository. Each project should contain .gitignore files to enforce this. It is also a good idea for each developer to have a global .gitignore file set as a fail safe.

* We shouldn't commit any dynamically generated files, for example: Drupal Image Style files, automatically Gzipped CSS/JS, Compiled CSS (from a preprocess such as Sass or LESS).
* Personal developer settings should not be under Git, i.e. local database settings, IDE project files etc.
* In the Git project root we shouldn't have any database backups (this poses a security risk).
* Any user uploaded files (e.g. the `sites/*/files` directory).
* OS specific files, such as `.DS_Store`.

## Development & Stage Environments
When working in large teams it is important that our individual work can come together in a widely accessible location. This allows for QA and internal and client demonstration. The following servers are used:

### New Development/Stage

#### *.d.ctidigital.com
* PHPMyadmin Interface:  [https://ctisql.d.ctidigital.com](https://ctisql.d.ctidigital.com)
* Files explorer interface: [https://files.d.ctidigital.com](https://files.d.ctidigital.com)
* Utils menu:  [http://util.d.ctidigital.com](http://util.d.ctidigital.com)

#### *.s.ctidigital.com
* PHPMyadmin Interface: [https://ctisql.s.ctidigital.com](https://ctisql.s.ctidigital.com)
* Files explorer interface: [https://files.s.ctidigital.com](https://files.s.ctidigital.com)
* Utils menu:  [http://util.s.ctidigital.com](http://util.s.ctidigital.com)

### Old Development/Stage

#### *.dev.ctidigital.com

* PHP Logs: [http://logs.stage.ctidigital.com](http://logs.stage.ctidigital.com)
* File Browser: [http://filebrowser.stage.ctidigital.com](http://filebrowser.stage.ctidigital.com)

#### *.stage.ctidigital.com
* PHPMyadmin: [http://phpmyadmin.stage.ctidigital.com](http://phpmyadmin.stage.ctidigital.com)
* PHP Logs: [http://logs.dev.ctidigital.com](http://logs.dev.ctidigital.com)
* File Browser: [http://filebrowser.dev.ctidigital.com](http://filebrowser.dev.ctidigital.com)

### Deploying Code
Code is deployed using Jenkins. This can be found at: [http://ci.ctidigital.com](http://ci.ctidigital.com)

## Drush
Drush is a command line interface for Drupal. It speeds up the development workflow by automating and reducing the many repetative tasks Drupal developers have to carry out. We make full use of Drush aliases so we are able to easily control the development and stage servers from our local command line. Each project contains specific Drush configuration in sites/all/drush, including aliases for the development and stage servers. This allows configuration to be shared between developers for each project.

## Features & Custom Modules

### Features
We aim for 100% code driven development. In Drupal, this means having no configuration in the database, instead exporting it into code with features. We use the following system for categorising configuration in features:

* **Feature Features** - These are single isolated _feature sets_ as may be defined in a specification, for example "News" could be a feature containing a node bundle, field bases and instances, strongarm variables for the content type, permissions and some views. On enabling this feature all the required functionality for this feature should be present.
* **Sitewide Features** - Many settings don't fit into any particular featureset. For these settings catchall features should be made, these could be "Roles and Permissions", "Menus", "Blocks", "Settings".

The package property on features can be used to manage these different types of features. At a minimum all features on a site should be split into `Features` and `Sitewide`.

**It is important to note that dependencies between features should be avoided.**

On the "Create Feature" page, features helpfully removes all items that are already in enabled features. This allows you to quickly check if everything is in features by going to the "Create Feature" page. Obviously, some configuration is environment specific or needent be version controlled (such as block positioning for disabled blocks).

If a module doesn't have features integration we have no way of deploying changes. In these cases we must add features intgration for that module and contribute this back to the community.


#### Including Custom Code in Features
Much of the code we write is small tweaks to configuration that relate to only a single feature of the site and doesn't warrent it's own module. Features are nothing more than plain Drupal modules, which is very powerful as it allows us to bundle feature specific custom code with the feature. This makes code easier to find and we can make more assumptions (such as the existance of a field) therefore reducing the dependencies we have to manage.

### Custom Modules
For adding additional functionality that is not configurable we must create custom modules. Custom modules should be standalone, and not dependent on any features. All settings specific to the site should be configurable and exportable into features (i.e. the feature should always depend on the module, not the other way around). By making modules as generic as possible it increases the opportunities for code reuse and contributions back to the community, while also making them easier to test. If a module relies on a feature, in most cases the code should be part of the feature.

## Patching
"Don't hack core" is a line that many people develop by, however in reality patching core/contributed code can be the best decision to make, from a business and developer sanity point of view. The one requirement for patching a module is that **patches must always have a drupal.org issue number and comment number**. This means that a bug or required feature has been found in the issue queues or the bug/feature warrants an issue being created and a patch posted. If it doesn't meet these criteria, patching probably isn't the correct solution.

### Patching Workflow
When patching, it is important we make it as obvious as possible which module has been patched, why and where we can find the current status of a patch. By doing this we make the update process as painless as possible. The following steps must be taken when patching a module:

1. Add the .patch file to the profiles/patches/ directory, using the drupal.org naming convention: [module-name]-[brief-description]-[issue-no]-[comment-no].patch
2. Add an entry into PATCHES.txt, alphabetically as follows:

		Auto Entity Label:
		------------------
		Add support for entity id tokens during creation.
		http://drupal.org/node/1445124#comment-7065964
		auto_entitylabel.info, auto_entitylabel.module
		auto_entitylabel-entity-id-support-1445124-12.patch
		[Committed]

3. Commit the all changes and the patch in a single commit.

## Sites as Installation Profiles
We build all our sites as installation profiles, so a site can be installed from just the codebase, without any reliance on a specific database. There are many benefits to this workflow:

* We are constantly working in a clean environment.
* Code driven development is forced upon us.
* There is no need to pass specific databases around.
* Test content can easily be shared and relied upon for automated testing.
* Simpletest `DrupalWebTestCase::setUp()` methods can be vastly simplified by setting the `DrupalWebTestCase::profile` property to the project installation profile acceptance testing.
* The installation of the profile can be used as a fixture for Behat tests.
* Features is very good at exporting configuration, however often there are problems using features to manage configuration.

There are some challenges in working this way however, mainly due to ensuring production content is kept intact during the re-installation process. We are able to get around this by specifying the content tables in the database in .content-tables file within the env/ directory. This lists the database tables that contain content and should therefore be kept intact during the build process. For example:

		node
		field_data_*
		field_revision_*
		comment

